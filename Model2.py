from imghdr import tests
from numpy import argmax
import torch
from torch import nn
import torch.utils.data as data
import torch.optim as optim
from torchvision import datasets
from torchvision.transforms import ToTensor, Lambda, Compose, PILToTensor
import matplotlib.pyplot as plt
from PIL import Image
from torchvision.io import read_image

# Get cpu or gpu device for training.
device = "cuda" if torch.cuda.is_available() else "cpu"
print("Using {} device".format(device))

# get training data.
trainSet= datasets.MNIST(root="dataset",train=True,download=False,transform=ToTensor(),)

# get test data.
testSet = datasets.MNIST(root="dataset", train=False, download=False,transform=ToTensor(),)



# Define model and tune stuff

#Model skeleton code from a pytorch examplle
class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_stack = nn.Sequential(
            nn.Linear(28*28, 100),
            nn.ReLU(),
            nn.Linear(100, 100),
            nn.ReLU(),
            nn.Linear(100, 100),
            nn.Sigmoid(),
            nn.Linear(100, 10)
        )
        
    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_stack(x)
        return logits

#Some more model paramters
model = NeuralNetwork().to(device)
learning_rate = 0.001
optimiser = torch.optim.SGD(model.parameters(), lr=learning_rate)
loss_function = torch.nn.CrossEntropyLoss()
batch_size = 60
epochs = 5
percentValidation = 0.1 #the fraction of the available training data to use as the validation set

#I wasn't using function initially but as I added the ability to train on the validation set it just made more sense to use functions
#Lots of borrowed code from the suggested pytorch example with only one real change 
def train_model(dataloader, model, loss_fn, optimiser):
    model.train() 
    size = len(dataloader.dataset)
    for batch, (input, truth) in enumerate(dataloader):
        input, truth = input.to(device), truth.to(device)

        # Compute prediction error/loss
        hypothesis = model(input)
        loss = loss_fn(hypothesis, truth)

        # Backpropagation
        optimiser.zero_grad() #Clear gradients
        loss.backward() #Compute Gradients
        optimiser.step() #Update weights

        #Print loss every 100 batches
        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(input)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

#lots of borrowed code from the suggested pytorch example with a few changes
def test_model(dataloader, model,test):
    model.eval() #put model into evaluation mode - not really necessary since we don't have any dropout layers
    test_loss, correct = 0, 0 #initialise/set loss and correct values to 0
    dataset_size = len(dataloader.dataset)   
    with torch.no_grad():
        for input, truth in dataloader: #Iterate through x and ground truth values
            input, truth = input.to(device), truth.to(device) 
            prediction = model(input) #Run input through the model and make a hypothesis
            test_loss += loss_function(prediction, truth).item() # Update loss from ground truth value
            correct += (prediction.argmax(1) == truth).type(torch.float).sum().item() #Update correct from ground truth value
    test_loss /= dataset_size #Divide loss value by dataset size to get average loss
    correct /= dataset_size #Divide correct value by dataset size to get Accuracy
    if test == True: #I just added this so I could have different print statements
        print(f"Test Accuracy: {(100*correct):>0.1f}% \n")
    else:
        print(f"Validation Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")
         

#Split into trainSet into actualTrainSet and validationSet - not really necessary to have a validation set for this NN but I wanted one
valSize = int(percentValidation * len(trainSet)) 
trainSize = len(trainSet) - valSize
valSset, actualTrainSet = torch.utils.data.random_split(trainSet, [valSize, trainSize])

# Create data loaders for train, validation, .
train_dataloader = data.DataLoader(actualTrainSet, batch_size=batch_size,shuffle=True)
validation_dataloader = data.DataLoader(valSset, batch_size=batch_size)
test_dataloader = data.DataLoader(testSet, batch_size=batch_size)

#train on training data
for t in range(epochs):
    print(f"Epoch {t+1}\n-------------------------------")
    train_model(train_dataloader, model, loss_function, optimiser)
    test_model(validation_dataloader, model,False)
print("Done training!\nbegin testing:")

#Check vs test set
test_model(test_dataloader,model,True)


#check if the user would like to train the model on the validation set
inputStr = ""
while inputStr!="y" and inputStr!="n" and inputStr!="Y" and inputStr!="N":

    inputStr = input("Would you like to train on the validation set as well? (y/n): \n")
    if inputStr == "y" or inputStr=="Y":

        #Train on validation Set for for complete model
        print("Now training on validation set: ")
        for t in range(epochs):
            print(f"Epoch {t+1}\n-------------------------------")
            train_model(validation_dataloader, model, loss_function, optimiser)
            test_model(validation_dataloader, model,False)
        print("Done training on validation set!\n begin testing again!:")

        #Check vs test set again
        test_model(test_dataloader,model,True)
    
#Allow user to enter a file path
inputStr = ""
model.eval()
while inputStr!="exit":
    inputStr = input("Please enter a filepath of a 28x28 image to run inference on or type exit to exit: \n")
    if inputStr == "exit":
        break
    try:
        image = Image.open(inputStr)  #read_image("MNIST_JPGS/testSample/img_1.jpg") Image.open("MNIST_JPGS/testSample/img_1.jpg")
    except:
        print("File not found")

    else:
        convert_tensor = ToTensor()
        tensor = convert_tensor(image)
        prediction= model(tensor)
        print("\n The learned machine decrees that this image represents:",argmax(prediction[0].detach().numpy()),"\n")
